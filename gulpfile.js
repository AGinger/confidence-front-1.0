const gulp = require('gulp');
const pug = require('gulp-pug');
const nunjucks = require('gulp-nunjucks');
const sass = require('gulp-sass');
const autoprefixer = require('gulp-autoprefixer')
const imagemin = require('gulp-imagemin');
const browserSync = require('browser-sync').create();

const del = require('del');
const cleanCSS = require('gulp-clean-css');
const gulpif = require('gulp-if');
const gcmq = require('gulp-group-css-media-queries');
const smartgrid = require('smart-grid');
const data = require('gulp-data');

const isDev = (process.argv.indexOf('--dev') !== -1);
const isProd = !isDev;
const isSync = (process.argv.indexOf('--sync') !== -1);

const folder = {
  src :'src/',
  dist:'dist/'
};

const path = {
  nunjucks: 'templates/**/*.html',
  pug:"templates/**/*.pug",
  sass: 'sass/**/*.scss',
  fonts: 'fonts/**/*',
  images: 'images/**/*',
  js: 'js/**/*',
  libs: 'libs/**/*'
};
const pugFiles = [ 
	'./src/templates/**/*.pug',
];

function clear() {
	return del(folder.dist);
}

function html() {
	return gulp.src(folder.src + path.nunjucks)
    .pipe(nunjucks.compile())
    .pipe(gulp.dest(folder.dist))
    .pipe(gulpif(isSync, browserSync.stream()))
    ;
}
function pugC() {
	
	return gulp.src(pugFiles)
				
		.pipe(data(file => require('./version-release.json')))
		.pipe(pug({
			 pretty: '\t',
		}))
		.pipe(gulp.dest('./src/templates/'))
		
		.pipe(gulpif(isSync, browserSync.stream()))
		// .pipe(notify({ message: 'pugc task complete' }));
		
}

function styles() {
  return gulp.src(folder.src + path.sass)
    .pipe(sass())
    .pipe(gcmq())
    .pipe(autoprefixer({
			//in package.json
    }))
    .pipe(gulpif(isProd, cleanCSS({
			level: {
          2: {
            all: true, // sets all values to `false`
          }
			  }
		})))
    .pipe(gulp.dest(folder.dist + 'css/'))
    .pipe(gulpif(isSync, browserSync.stream()));
}

function scripts() {
  return gulp.src(folder.src + path.js)
    .pipe(gulp.dest(folder.dist + 'js'))
    .pipe(gulpif(isSync, browserSync.stream()));
}

function fonts() {
  return gulp.src(folder.src + path.fonts)
    .pipe(gulp.dest(folder.dist + 'fonts'))
}

function images() {
  return gulp.src(folder.src + path.images)
  .pipe(gulp.dest(folder.dist + 'images'))
  .pipe(gulpif(isSync, browserSync.stream()));
}

function libs() {
  return gulp.src(folder.src + path.libs)
    .pipe(gulp.dest(folder.dist + 'libs'))
    .pipe(browserSync.stream());
}

function grid(done) {
	delete require.cache[require.resolve('./smartgrid.js')];

	let settings = require('./smartgrid.js');
	smartgrid('./src/sass/smartgrid', settings);

	settings.offset = '3.1%';
	settings.filename = 'smart-grid-per';
	smartgrid('./src/sass/smartgrid', settings);

	done();
}

function watch() {
	if (isSync) {
		browserSync.init({
			port: 4200,
			server: {
				baseDir: "./dist",
			}
			// tunnel: true //использование локального сервера
		});
	}

  gulp.watch(folder.src + path.sass, styles);
  gulp.watch(folder.src + path.nunjucks, html);
  gulp.watch(pugFiles, pugC);
	gulp.watch(folder.src + path.js, scripts);

	gulp.watch('./smartgrid.js', grid);
}


let build = gulp.series(clear,
	gulp.parallel(html,pugC, styles, scripts, images, fonts, libs)
);

gulp.task('build', gulp.series(grid, build));
gulp.task('watch', gulp.series(build, watch));
gulp.task('grid', grid);

