$(document).ready(function () {
    function scrollTo(selector) {
        document.querySelector(selector).scrollIntoView({ behavior: 'smooth' })
    }
    $(window).on('load scroll', function() {
        var $footer = $('.footer');
        var $footerBot = $('.footer__bot');
        var windowWidth = $(window).width();
        var windowHeight = $(window).height();
        var documentHeight = $('body').height();
        var headerHeight = $header.outerHeight();
        var footerHeight = $footerBot.outerHeight();
        var menuVisible = $('.header .menu-toggle')
        var topOffset = $(document).scrollTop(),
          toBottom = topOffset - (documentHeight - windowHeight - footerHeight);
    
       
    
        headerHeight = $header.outerHeight();
    
        if (toBottom > 0) {
          $('.bottom-nav').css('bottom', toBottom);
          $('body').addClass('document-bottom');
        } else {
          $('.bottom-nav').css('bottom', 0);
          $('body').removeClass('document-bottom');
        }
      });

    $('.menu-btn-mobile').click(function () {
        $('.menu-btn-mobile').toggleClass('active');
        $('.header__nav').toggleClass('active');
    });

    


    (function () {
        $('.counter__plus').click(function () {
            $(this).prev().get(0).value++;
        })
        $('.counter__minus').click(function () {
            $(this).next().get(0).value--;
            if ($(this).next().get(0).value < 0) {
                $(this).next().val(0)
            }
        })
    })();

    $('.imageGallery a').simpleLightbox();

    $('.customers-slider').slick({
        arrows: false,
        dots: true
    });

    $('.articles-slider').slick({
        // arrows: false,
        dots: true
    });

    $('.services-slider__slider').slick({
        slidesToShow: 3,
        arrows: true,
        responsive: [{
                breakpoint: 768,
                settings: {
                    slidesToShow: 2,
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                }
            }
        ]
    });

    $('.testimonials__slider').slick({
        dots: true
    });

    $('.team__slider').slick({
        infinite: false,
       
        responsive: [{
            breakpoint: 564,
            settings: {
                initialSlide: 1,
            }
        }
    ]
    });


    $('.serts-slider').slick({
        dots: true
    });

    (function stepsSlider() {
        let stepsSlider = $('.steps-slider__body');
        if (!stepsSlider.length || $(window).width() < 768) {
            return
        }

        stepsSlider.slick({
            dots: true,
            arrows: false,
            customPaging: function (slick, i) {
                const imgPaths = ['images/steps-slider/11.svg', 'images/steps-slider/22.svg', 'images/steps-slider/33.svg', 'images/steps-slider/44.svg', 'images/steps-slider/55.svg', 'images/steps-slider/66.svg'];
                const captions = ['Оформление заявки', 'Подготовка документов', 'Подача документов', 'Получение документов', 'Оплата', 'Гарантийное обслуживание'];

                return (`
                <div class="customDot-js white">
                    <div class="customDot-js__img">
                        <img src='${imgPaths[i]}'/>
                    </div>
                    <div class="customDot-js__caption">
                        ${captions[i]}
                    </div>
                <div>
                `);
            }
        });

    }());

    (function stepsAccordionMobile() {
        if ($(window).width() > 768) {
            return
        }

        const head = $('.mobile-accordion-head');
        const body = $('.mobile-accordion-body')

        head.click(function (e) {
            $(this).parent().toggleClass('active')
        });


    }());

    var mainVideo = $('#hero-video');

    function appendResponsiveVideo(width) {
        mainVideo.append(`<source src="images/video/confidence-${width}.mp4" type="video/mp4" >`);
        mainVideo.append(`<source src="images/video/confidence-${width}.flv" type="video/flv" >`);
        mainVideo.append(`<source src="images/video/confidence-${width}.ogv" type="video/ogv" >`);
        mainVideo.append(`<source src="images/video/confidence-${width}.webm" type="video/webm" >`);
    }
    function appendResponsiveVideoPoster(width){
        mainVideo.attr('poster', `images/${width}/video-prompt-${width}.jpg`);
    }
    if ($(window).width() >= 1440) {
        appendResponsiveVideoPoster(1440);
        appendResponsiveVideo(1920);
    } else if ($(window).width() >= 1280) {
        appendResponsiveVideoPoster(1280);
        appendResponsiveVideo(640);
    } else if ($(window).width() > 1024) {
        appendResponsiveVideo(640);
    } else if ($(window).width() >= 768) {
        appendResponsiveVideoPoster(768);
    } else if ($(window).width() >= 564) {
        appendResponsiveVideoPoster(564);
    } else {
        return false;
    }
    
    $header = $('.header'),
    headerHeight = $header.outerHeight(),
    
    $('.bottom-nav a.next').click(function(e) {
        e.preventDefault(); 
        var topOffset = $(document).scrollTop();
        if ($('body').hasClass('document-bottom')) {
            scrollTo('.content');
        } else {
        $('[data-slide="slideTo"]').each(function() {
            var slideTop = Math.round($(this).offset().top - headerHeight);
            if (slideTop > topOffset) {
                if ($(this).is('.footer')) {
                    $('body').addClass('document-bottom');
                }
                scrollTo('.'+this.classList[0]);
                return false;
            }
        });
        }
    });
});


// yutube embed
document.addEventListener("DOMContentLoaded",
    function () {
        var div, n,
            v = document.getElementsByClassName("youtube-player");
        for (n = 0; n < v.length; n++) {
            div = document.createElement("div");
            div.setAttribute("data-id", v[n].dataset.id);
            div.innerHTML = labnolThumb(v[n].dataset.id);
            div.onclick = labnolIframe;
            v[n].appendChild(div);
        }
    });

function labnolThumb(id) {
    var thumb = '<img src="https://i.ytimg.com/vi/ID/hqdefault.jpg">',
        play = '<div class="play"></div>';
    return thumb.replace("ID", id) + play;
}

function labnolIframe() {
    var iframe = document.createElement("iframe");
    iframe.setAttribute("src", "https://www.youtube.com/embed/" + this.dataset.id + "?autoplay=1");
    iframe.setAttribute("frameborder", "0");
    iframe.setAttribute("allowfullscreen", "1");
    this.parentNode.replaceChild(iframe, this);
}

const modalReviews = document.getElementById('modal-reviews');
const modalReviewsBtn = document.querySelector('[data-modal="modal-reviews"]');
if(modalReviewsBtn){
    modalReviewsBtn.addEventListener('click', function(){
        if(modalReviews.style.display !== "none"){
            let btndots = document.querySelector('.slick-dots .slick-active');
            btndots.click();
            setTimeout(()=>{document.querySelector('.testimonials__slider').style.opacity = '1';},500);
        }
    });
}

const eventPrint = document.querySelector('.event-info__item--print');

if(eventPrint){
    eventPrint.onclick = function(){
        window.print()
    }
}


// Safari webp fix

$(document).ready(function() {

    var isSafari = /^((?!chrome|android).)*safari/i.test(navigator.userAgent);
    
    if (isSafari) {
        
         $('.safari-fix').each(function() {
            var src = $(this).attr('src');
            var format = $(this).data('format');
            src = src.replace('.webp', '.' + format);
            $(this).attr('src', src);
        });

    }

});